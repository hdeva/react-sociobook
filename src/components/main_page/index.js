import React from "react";

import './index.css';
import Footer from "../common/Footer";
import NavBar from "../common/NavBar";
import LeftSidebar from "./dependencies/LeftSidebar";
import RightSidebar from "./dependencies/RightSidebar";
import MainComponent from "./dependencies/MainComponent";

/**
 * This is a class representing the main page of the application. 
 */
class Main extends React.Component {

  render() {

    return (
      <>
        <NavBar />
        <div className="container">
          <LeftSidebar />
          <MainComponent />
          <RightSidebar />
        </div>
        <Footer />
      </>
    )
  }

}

export default Main